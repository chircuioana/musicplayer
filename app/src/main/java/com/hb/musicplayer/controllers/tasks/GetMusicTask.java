package com.hb.musicplayer.controllers.tasks;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import com.hb.musicplayer.ui.events.MusicFinishedLoadingEvent;
import com.hb.musicplayer.ui.listeners.OnMusicLoadedListener;
import com.hb.musicplayer.models.Song;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by HB on 09.08.2017.
 */

public class GetMusicTask extends AsyncTask<Void, Void, ArrayList<Song>> {

    private Context context;
    private OnMusicLoadedListener listener;


    public GetMusicTask(Context context, OnMusicLoadedListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected ArrayList<Song> doInBackground(Void... voids) {
        ArrayList<Song>songs=new ArrayList<>();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        ContentResolver cr = context.getContentResolver();
        final String[] columns = {MediaStore.Audio.Media._ID,
                MediaStore.Audio.Albums.ALBUM_ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DURATION};
        Cursor cursor = cr.query(uri, columns, null, null, MediaStore.Audio.Media.TITLE+" ASC");
        while (cursor.moveToNext()) {
            int album_column_index = cursor.getColumnIndexOrThrow(android.provider.MediaStore.Audio.Albums.ALBUM_ID);
            int albumID = cursor.getInt(album_column_index);
            Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri artUri = ContentUris.withAppendedId(sArtworkUri, albumID);

            int idColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);
            long thisId = cursor.getLong(idColumn);
            Uri idUri = ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, thisId);

            String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE));
            String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST));
            long duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION));
            songs.add(new Song(String.valueOf(thisId),title, artist, artUri.toString(),duration));
        }
        cursor.close();
        return songs;
    }

    @Override
    protected void onPostExecute(ArrayList<Song> songs) {
        super.onPostExecute(songs);
        listener.onMusicFinishedLoading(songs);
        EventBus.getDefault().post(new MusicFinishedLoadingEvent());
    }
}
