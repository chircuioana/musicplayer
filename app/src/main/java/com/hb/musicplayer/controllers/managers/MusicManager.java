package com.hb.musicplayer.controllers.managers;

import android.content.Context;
import android.support.v4.media.MediaMetadataCompat;

import com.hb.musicplayer.models.Song;
import com.hb.musicplayer.controllers.tasks.GetMusicTask;
import com.hb.musicplayer.ui.events.MusicLoadedEvent;
import com.hb.musicplayer.ui.listeners.OnMusicLoadedListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by HB on 19.08.2017.
 */

public class MusicManager implements OnMusicLoadedListener {

    private static MusicManager instance;

    private MusicManager(Context context) {
        this.context = context.getApplicationContext();
        currentPlaylist = new ArrayList<>();
        songsMap = new HashMap<>();
        mediaMetaHash = new HashMap<>();
    }

    public static synchronized MusicManager getInstance(Context context) {
        if (instance == null) {
            instance = new MusicManager(context);
        }
        return instance;
    }

    private Context context;
    private HashMap<String, MediaMetadataCompat> mediaMetaHash;
    private HashMap<String, Song> songsMap;
    private ArrayList<Song> currentPlaylist;
    private String currentPlayingSongId;

    public void getSongsTask() {
        new GetMusicTask(context, this).execute(null, null, null);
    }

    @Override
    public void onMusicFinishedLoading(ArrayList<Song> songs) {
        for (Song song : songs) {
            songsMap.put(song.getJustId(), song);
            mediaMetaHash.put(song.getJustId(), getMetaItem(song));
        }
        this.currentPlaylist = new ArrayList<>(songs);
        EventBus.getDefault().post(new MusicLoadedEvent());
    }

    public HashMap<String, Song> getSongsMap() {
        return songsMap;
    }

    public ArrayList<Song> getCurrentPlaylist() {
        return currentPlaylist;
    }

    private MediaMetadataCompat getMetaItem(Song song) {
        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, song.getJustId())
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_URI, song.getId())
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, song.getArtist())
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, song.getDuration())
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, song.getCover())
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, song.getTitle())
                .build();

    }

    public HashMap<String, MediaMetadataCompat> getMediaMetaHash() {
        return mediaMetaHash;
    }

    public String getCurrentPlayingSongId() {
        if (currentPlayingSongId != null) {
            return currentPlayingSongId;
        }
        return "";
    }

    public void setCurrentPlayingSongId(String currentPlayingSongId) {
        this.currentPlayingSongId = currentPlayingSongId;
    }
}
