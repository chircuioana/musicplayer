package com.hb.musicplayer.ui.listeners;

import com.hb.musicplayer.models.Song;

/**
 * Created by HB on 13.08.2017.
 */

public interface OnSongClickedListener {
    void onSongClicked(Song song);
}
