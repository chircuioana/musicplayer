package com.hb.musicplayer.ui.fragments;

import android.content.ComponentName;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hb.musicplayer.controllers.managers.MusicService;
import com.hb.musicplayer.R;
import com.hb.musicplayer.controllers.managers.MusicManager;
import com.hb.musicplayer.models.Song;
import com.hb.musicplayer.ui.activities.ParrentActivity;
import com.hb.musicplayer.ui.adapters.MusicAdapter;
import com.hb.musicplayer.ui.events.MusicLoadedEvent;
import com.hb.musicplayer.ui.events.SongChangedEvent;
import com.hb.musicplayer.ui.listeners.OnChangeFragmentListener;
import com.hb.musicplayer.ui.listeners.OnSongClickedListener;
import com.hb.musicplayer.ui.utils.OverlapDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HB on 13.08.2017.
 */

public class SongListFragment extends BrowseFragment implements OnSongClickedListener {
    public static SongListFragment newInstance() {

        Bundle args = new Bundle();
        SongListFragment fragment = new SongListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rvMusicList)
    RecyclerView rvMusicList;

    private ArrayList<Song> songs;
    private MusicAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_song_list, null);
        ButterKnife.bind(this, view);
        init();
        mMediaBrowser = new MediaBrowserCompat(getActivity(),
                new ComponentName(getActivity(), MusicService.class),
                mConnectionCallback, null);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        refresh();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void init() {
        songs = new ArrayList<>();
        adapter = new MusicAdapter(getActivity(), songs, this);
        rvMusicList.setAdapter(adapter);
        rvMusicList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMusicList.addItemDecoration(new OverlapDecoration());
    }

    private void refresh() {
        songs = MusicManager.getInstance(getActivity()).getCurrentPlaylist();
        if (songs != null && songs.size() > 0) {
            adapter.setSongs(songs);
            adapter.notifyDataSetChanged();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void MusicLoadedEvent(MusicLoadedEvent event) {
        refresh();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void SongChangedEvent(SongChangedEvent event) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSongClicked(Song song) {
        ((ParrentActivity) getActivity()).onChangeFragment(OnChangeFragmentListener.FRAGMENT.SONG, SongFragment.newInstance(song));
    }
}
