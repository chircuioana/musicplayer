package com.hb.musicplayer.ui.listeners;

import android.support.v4.app.Fragment;

/**
 * Created by HB on 13.08.2017.
 */

public interface OnChangeFragmentListener {
    public enum FRAGMENT{
        SONG_LIST,
        SONG,
    }

    void onChangeFragment(FRAGMENT fragmentType, Fragment fragment);
}
