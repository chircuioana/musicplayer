package com.hb.musicplayer.ui.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cleveroad.audiovisualization.DbmHandler;
import com.cleveroad.audiovisualization.GLAudioVisualizationView;
import com.cleveroad.audiovisualization.VisualizerDbmHandler;
import com.hb.musicplayer.controllers.managers.MusicManager;
import com.hb.musicplayer.ui.enums.PlayerLayoutState;
import com.hb.musicplayer.ui.events.ChangePlayerStateEvent;
import com.hb.musicplayer.ui.events.PalleteEvent;
import com.hb.musicplayer.ui.events.PreparedEvent;
import com.hb.musicplayer.ui.fragments.SongListFragment;
import com.hb.musicplayer.ui.listeners.OnChangeFragmentListener;
import com.hb.musicplayer.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.hb.musicplayer.ui.enums.PlayerLayoutState.MINIMIZED;
import static com.hb.musicplayer.ui.enums.PlayerLayoutState.OFF;
import static com.hb.musicplayer.ui.enums.PlayerLayoutState.ON;

public class ParrentActivity extends AppCompatActivity implements OnChangeFragmentListener {

    private static final int REQUEST_PERMISSIONS = 1001;

    @BindView(R.id.flFragmentLayout)
    FrameLayout flFragmentLayout;

    @BindView(R.id.flPlayingLayout)
    FrameLayout flPlayingLayout;

    @BindView(R.id.visualizer_view)
    GLAudioVisualizationView visualizationView;

    @BindView(R.id.tbMain)
    Toolbar toolbar;

    @BindView(R.id.llMainLayout)
    LinearLayout llMainLayout;

    @BindView(R.id.rlParent)
    RelativeLayout rlParent;

    @BindView(R.id.tvNoPermission)
    TextView tvNoPermission;

    VisualizerDbmHandler vizualizerHandler;
    private PlayerLayoutState playerState;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
        checkPermissions();
        showSongList();
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean hasReadPermission = true;
            ArrayList<String> permissions = new ArrayList<>();
            if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.RECORD_AUDIO);
            }
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                hasReadPermission = false;
            }
            if (permissions.size() > 0) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_PERMISSIONS);
            }
            if (!hasReadPermission) {
                return;
            }
        }
        MusicManager.getInstance(this).getSongsTask();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS) {
            for (int i = 0; i < permissions.length; i++) {
                switch (permissions[i]) {
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            MusicManager.getInstance(this).getSongsTask();
                        } else {
                            tvNoPermission.setVisibility(View.VISIBLE);
                        }
                        break;
                    case Manifest.permission.RECORD_AUDIO:

                        break;
                }
            }
        }
    }

    private void init() {
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        playerState = OFF;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        visualizationView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        visualizationView.onPause();
    }

    @Override
    protected void onDestroy() {
        visualizationView.release();
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPreparedEvent(PreparedEvent event) {
        this.mediaPlayer = event.mediaPlayer;
        setupVizualizer();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGeneratePallete(PalleteEvent event) {
        Log.e("COLORS", event.wave1 + " " + event.wave2 + " " + event.wave3 + " " + event.wave4);
        final GLAudioVisualizationView oldViz = visualizationView;

        visualizationView = new GLAudioVisualizationView.Builder(this)
                .setBubblesSize(R.dimen.bubble_size)
                .setBubblesRandomizeSize(true)
                .setWavesHeight(R.dimen.wave_height)
                .setWavesFooterHeight(R.dimen.footer_height)
                .setWavesCount(50)
                .setLayersCount(4)
                .setBackgroundColorRes(R.color.colorPrimary)
                .setLayerColors(new int[]{event.wave1, event.wave2, event.wave3, event.wave4})
                .setBubblesPerLayer(16)
                .build();
        rlParent.addView(visualizationView, 0);
        rlParent.removeView(oldViz);

        if (mediaPlayer != null) {
            setupVizualizer();
        }
    }

    private void setupVizualizer() {
        vizualizerHandler = DbmHandler.Factory.newVisualizerHandler(this, mediaPlayer.getAudioSessionId());
        visualizationView.linkTo(vizualizerHandler);
        vizualizerHandler.onPrepared(mediaPlayer);
//        vizualizerHandler.onCompletion(mediaPlayer);
    }

    private void showSongList() {
        startFragment(SongListFragment.newInstance());
    }

    private void startFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flFragmentLayout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onChangeFragment(FRAGMENT fragmentType, Fragment fragment) {
        switch (fragmentType) {
            case SONG_LIST:
                startFragment(fragment);
                break;
            case SONG:
                PlayerLayoutState newState = playerState == OFF ? ON : MINIMIZED;
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                fragment.getArguments().putSerializable("STATE_KEY", newState);
                transaction.replace(R.id.flPlayingLayout, fragment);
                transaction.commit();
                if (newState != MINIMIZED) {
                    setPlayerState(newState);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (playerState == ON) {
//            setPlayerState(MINIMIZED);
            EventBus.getDefault().post(new ChangePlayerStateEvent(MINIMIZED));
            return;
        }
        if (playerState == MINIMIZED) {
            setPlayerState(OFF);
            return;
        }
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }

    public void setPlayerState(PlayerLayoutState playerState) {
        this.playerState = playerState;
        ViewGroup.LayoutParams layoutParams = flPlayingLayout.getLayoutParams();
        switch (playerState) {
            case ON:
                layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
                flPlayingLayout.setLayoutParams(layoutParams);
                flPlayingLayout.setVisibility(View.VISIBLE);
                llMainLayout.setVisibility(View.GONE);
                break;
            case MINIMIZED:
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                flPlayingLayout.setLayoutParams(layoutParams);
                llMainLayout.setVisibility(View.VISIBLE);
                break;
            case OFF:
                flPlayingLayout.setVisibility(View.GONE);
                llMainLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    public PlayerLayoutState getPlayerState() {
        return playerState;
    }
}
