package com.hb.musicplayer.ui.enums;

/**
 * Created by HB on 20.08.2017.
 */

public enum PlayerLayoutState{
    ON,
    MINIMIZED,
    OFF
}
