package com.hb.musicplayer.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hb.musicplayer.R;
import com.hb.musicplayer.controllers.managers.MusicManager;
import com.hb.musicplayer.models.Song;
import com.hb.musicplayer.ui.activities.ParrentActivity;
import com.hb.musicplayer.ui.enums.PlayerLayoutState;
import com.hb.musicplayer.ui.events.ChangePlayerStateEvent;
import com.hb.musicplayer.ui.events.PauseEvent;
import com.hb.musicplayer.ui.events.PlayEvent;
import com.hb.musicplayer.ui.events.SongCompleteEvent;
import com.hb.musicplayer.ui.events.SongTimerEvent;
import com.hb.musicplayer.ui.utils.DateUtils;
import com.hb.musicplayer.ui.views.VinylView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.hb.musicplayer.ui.enums.PlayerLayoutState.ON;

/**
 * Created by HB on 13.08.2017.
 */

public class SongFragment extends Fragment implements View.OnClickListener {

    public static final String SONG_KEY = "SONG";

    public static SongFragment newInstance(Song song) {
        Bundle args = new Bundle();
        args.putSerializable(SONG_KEY, song);
        SongFragment fragment = new SongFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.llParent)
    LinearLayout llParent;
    @BindView(R.id.tbPlayer)
    Toolbar toolbar;
    @BindView(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;
    @BindView(R.id.tvToolbarArtist)
    TextView tvToolbarArtist;
    @BindView(R.id.ivToolbarMinimize)
    ImageView ivToolbarMinimize;
    @BindView(R.id.ivToolbarMaximize)
    ImageView ivToolbarMaximize;
    @BindView(R.id.pbSongProgress)
    ProgressBar pbSongProgress;

    @BindView(R.id.rlSeekBar)
    RelativeLayout rlSeekBar;
    @BindView(R.id.rlVinylVisual)
    RelativeLayout rlVinylVisuals;
    @BindView(R.id.rlControls)
    RelativeLayout rlControls;

    @BindView(R.id.ivVinyl)
    VinylView ivVinyl;
    @BindView(R.id.fabPlayPause)
    FloatingActionButton fabPlayPause;
    @BindView(R.id.ivPrevious)
    ImageView ivPrevious;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    @BindView(R.id.tvCurrentTime)
    TextView tvCurrentTime;
    @BindView(R.id.tvFinishTime)
    TextView tvFinishTime;
    @BindView(R.id.sbSongProgress)
    SeekBar sbSongProgress;

    private Song song;
    private ArrayList<Song> playlist;
    private int currentPosition;
    private boolean isPlaying;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_song, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        ivPrevious.setOnClickListener(this);
        ivNext.setOnClickListener(this);
        fabPlayPause.setOnClickListener(this);
        ivToolbarMinimize.setOnClickListener(this);
        ivToolbarMaximize.setOnClickListener(this);
        ivVinyl.setShouldGeneratePallete(true);

        song = (Song) getArguments().get(SONG_KEY);
        playlist = new ArrayList<>(MusicManager.getInstance(getActivity()).getCurrentPlaylist());
        currentPosition = playlist.indexOf(song);
        setSong(song);
        PlayerLayoutState state = (PlayerLayoutState) getArguments().get("STATE_KEY");
        if (state == PlayerLayoutState.MINIMIZED) {
            minimize();
        }
    }

    public void setSong(Song song) {
        tvToolbarTitle.setText(song.getTitle());
        tvToolbarArtist.setText(song.getArtist());
        sbSongProgress.setMax((int) song.getDuration());
        pbSongProgress.setMax((int) song.getDuration());
        sbSongProgress.setProgress(0);
        sbSongProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    changeTime(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        tvFinishTime.setText(DateUtils.convertDuration(song.getDuration()));
        ivVinyl.setCover(Uri.parse(song.getCover()));
        startSong(song);
    }

    public void setPlaylist(ArrayList<Song> playlist) {
        this.playlist = playlist;
    }

    private void startSong(Song song) {
        MediaControllerCompat controller = MediaControllerCompat.getMediaController(getActivity());
        MediaControllerCompat.TransportControls controls = controller.getTransportControls();
        controls.playFromMediaId(song.getJustId(), null);
    }

    private void playPause() {
        MediaControllerCompat controller = MediaControllerCompat.getMediaController(getActivity());
        MediaControllerCompat.TransportControls controls = controller.getTransportControls();
        if (isPlaying) {
            controls.pause();
        } else {
            controls.play();
        }
    }

    private void changeTime(int time) {
        MediaControllerCompat controller = MediaControllerCompat.getMediaController(getActivity());
        MediaControllerCompat.TransportControls controls = controller.getTransportControls();
        controls.seekTo(time);
        Log.e("Seek time", time + "");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabPlayPause:
                playPause();
                break;
            case R.id.ivNext:
                playNext();
                break;
            case R.id.ivPrevious:
                playPrevious();
                break;
            case R.id.ivToolbarMinimize:
                makeSmaller();
                break;
            case R.id.ivToolbarMaximize:
                maximize();
                break;
        }
    }

    private void playNext() {
        currentPosition++;
        setSong(playlist.get((playlist.size() + currentPosition) % playlist.size()));
    }

    private void playPrevious() {
        currentPosition--;
        setSong(playlist.get((playlist.size() + currentPosition) % playlist.size()));
    }

    private void makeSmaller() {
        if (((ParrentActivity) getActivity()).getPlayerState() == ON) {
            minimize();
        } else {
            ((ParrentActivity) getActivity()).setPlayerState(PlayerLayoutState.OFF);
        }

    }

    private void minimize() {
        rlSeekBar.setVisibility(View.GONE);
        rlVinylVisuals.setVisibility(View.GONE);
        pbSongProgress.setVisibility(View.VISIBLE);
        ivToolbarMaximize.setVisibility(View.VISIBLE);
//        llParent.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.colorTint));
        ((ParrentActivity) getActivity()).setPlayerState(PlayerLayoutState.MINIMIZED);
    }

    private void maximize() {
        rlSeekBar.setVisibility(View.VISIBLE);
        rlVinylVisuals.setVisibility(View.VISIBLE);
        pbSongProgress.setVisibility(View.GONE);
        ivToolbarMaximize.setVisibility(View.GONE);
        ((ParrentActivity) getActivity()).setPlayerState(ON);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlayEvent(PlayEvent event) {
        isPlaying = true;
        fabPlayPause.setImageResource(R.drawable.ic_pause_white_24dp);
        ivVinyl.resumeAnim();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPauseEvent(PauseEvent event) {
        isPlaying = false;
        fabPlayPause.setImageResource(R.drawable.ic_play_arrow_white_24dp);
        ivVinyl.pauseAnim();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSongTimer(SongTimerEvent event) {
        tvCurrentTime.setText(DateUtils.convertDuration(event.time));
        sbSongProgress.setProgress((int) event.time);
        pbSongProgress.setProgress((int) event.time);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSongComplete(SongCompleteEvent event) {
        playNext();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangePlayerState(ChangePlayerStateEvent event) {
        switch (event.state) {
            case MINIMIZED:
                makeSmaller();
                break;
        }
    }

}
