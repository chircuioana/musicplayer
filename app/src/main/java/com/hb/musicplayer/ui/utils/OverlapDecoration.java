package com.hb.musicplayer.ui.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by HB on 09.08.2017.
 */

public class OverlapDecoration extends RecyclerView.ItemDecoration {

    private final static int vertOverlap = -200;

    @Override
    public void getItemOffsets (Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(0, vertOverlap, 0, 0);
    }
}
