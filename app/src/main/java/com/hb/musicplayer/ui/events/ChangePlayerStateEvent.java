package com.hb.musicplayer.ui.events;

import com.hb.musicplayer.ui.enums.PlayerLayoutState;

/**
 * Created by HB on 21.08.2017.
 */

public class ChangePlayerStateEvent {
    public PlayerLayoutState state;

    public ChangePlayerStateEvent(PlayerLayoutState state) {
        this.state = state;
    }
}
