package com.hb.musicplayer.ui.listeners;

import com.hb.musicplayer.models.Song;

import java.util.ArrayList;

/**
 * Created by HB on 09.08.2017.
 */

public interface OnMusicLoadedListener {
   void onMusicFinishedLoading(ArrayList<Song>songs);
}
