package com.hb.musicplayer.ui.views;

import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.PlaybackStateCompat;
import android.widget.RemoteViews;

import com.hb.musicplayer.R;
import com.hb.musicplayer.models.Song;

/**
 * Created by HB on 21.08.2017.
 */

public class NotificationView extends RemoteViews {

    private Context context;

    public NotificationView(Context context, String packageName, int layoutId, MediaDescriptionCompat song, boolean isPlaying) {
        super(packageName, layoutId);
        this.context = context;
        setTextViewText(R.id.tvToolbarTitleNotification, song.getTitle());
        setTextViewText(R.id.tvToolbarArtistNotification, song.getSubtitle());
//        setImageViewResource(R.id.fabPlayPauseNotification, isPlaying ? R.drawable.ic_pause_white_24dp : R.drawable.ic_play_arrow_white_24dp);

        PendingIntent pendingIntent = isPlaying ?
                MediaButtonReceiver.buildMediaButtonPendingIntent(context, PlaybackStateCompat.ACTION_PAUSE) :
                MediaButtonReceiver.buildMediaButtonPendingIntent(context, PlaybackStateCompat.ACTION_PLAY);
        setOnClickPendingIntent(R.id.fabPlayPauseNotification, pendingIntent);
//        setOnClickPendingIntent(R.id.ivNextNotification, MediaButtonReceiver.buildMediaButtonPendingIntent(context, PlaybackStateCompat.ACTION_SKIP_TO_NEXT));
//        setOnClickPendingIntent(R.id.ivPreviousNotification, MediaButtonReceiver.buildMediaButtonPendingIntent(context, PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS));

    }
}
