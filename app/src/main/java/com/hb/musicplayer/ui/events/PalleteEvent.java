package com.hb.musicplayer.ui.events;

/**
 * Created by HB on 21.08.2017.
 */

public class PalleteEvent {
    public int wave1;
    public int wave2;
    public int wave3;
    public int wave4;

    public PalleteEvent(int wave1, int wave2, int wave3, int wave4) {
        this.wave1 = wave1;
        this.wave2 = wave2;
        this.wave3 = wave3;
        this.wave4 = wave4;
    }
}
