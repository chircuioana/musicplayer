package com.hb.musicplayer.ui.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hb.musicplayer.controllers.managers.MusicManager;
import com.hb.musicplayer.ui.listeners.OnSongClickedListener;
import com.hb.musicplayer.R;
import com.hb.musicplayer.models.Song;
import com.hb.musicplayer.ui.views.VinylView;

import java.util.ArrayList;

/**
 * Created by HB on 09.08.2017.
 */

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Song> songs;
    private OnSongClickedListener listener;

    public MusicAdapter(Context context, ArrayList<Song> songs, OnSongClickedListener listener) {
        this.context = context;
        this.songs = new ArrayList<>(songs);
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_music, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setSong(songs.get(position));
    }

    @Override
    public int getItemCount() {
        if (songs != null) {
            return songs.size();
        }
        return 0;
    }

    public void setSongs(ArrayList<Song> songs) {
        this.songs = new ArrayList<>(songs);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private VinylView cover;
        private TextView title;
        private TextView artist;

        public ViewHolder(View itemView) {
            super(itemView);
            cover = (VinylView) itemView.findViewById(R.id.ivMusicPhoto);
            title = (TextView) itemView.findViewById(R.id.tvSongTitle);
            artist = (TextView) itemView.findViewById(R.id.tvSongArtist);
            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSongClicked(songs.get(getAdapterPosition()));
                }
            });
        }

        void setSong(Song song) {
            title.setText(song.getTitle());
            artist.setText(song.getArtist());
            cover.setCover(Uri.parse(song.getCover()));

            if (MusicManager.getInstance(context).getCurrentPlayingSongId().equals(song.getJustId())) {
                cover.resumeAnim();
            }else{
                cover.pauseAnim();
            }
        }
    }
}
