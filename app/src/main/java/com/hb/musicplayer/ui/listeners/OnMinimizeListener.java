package com.hb.musicplayer.ui.listeners;

/**
 * Created by HB on 20.08.2017.
 */

public interface OnMinimizeListener {
    void onMinimize();
}
