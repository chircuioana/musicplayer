package com.hb.musicplayer.ui.utils;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by HB on 19.08.2017.
 */

public class DateUtils {

    public static String convertDuration(long duration) {
        String out=String.format(Locale.ENGLISH,"%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(duration) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        return out;
    }
}
