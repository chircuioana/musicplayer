package com.hb.musicplayer.ui.events;

import android.media.MediaPlayer;

/**
 * Created by HB on 14.08.2017.
 */

public class PreparedEvent {
    public MediaPlayer mediaPlayer;

    public PreparedEvent(MediaPlayer mediaPlayer){
        this.mediaPlayer=mediaPlayer;
    }
}
