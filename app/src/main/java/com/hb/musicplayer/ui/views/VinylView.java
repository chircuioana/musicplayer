package com.hb.musicplayer.ui.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.graphics.Palette;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hb.musicplayer.R;
import com.hb.musicplayer.ui.events.PalleteEvent;
import com.hb.musicplayer.ui.utils.CircleTransform;

import org.greenrobot.eventbus.EventBus;

import static android.R.attr.bitmap;
import static android.os.Build.VERSION_CODES.M;
import static com.hb.musicplayer.R.color.wave2;

/**
 * Created by HB on 13.08.2017.
 */

public class VinylView extends RelativeLayout {

    private View view;
    private ImageView cover;
    private ObjectAnimator rotateAnimation;
    private Context context;
    private boolean shouldGeneratePallete;

    public VinylView(Context context) {
        super(context);
        init(context);
    }

    public VinylView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VinylView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_vinyl, null);
        cover = (ImageView) view.findViewById(R.id.ivVinyl);
        initAnimation();

        this.addView(view, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    private void initAnimation() {
        rotateAnimation = ObjectAnimator.ofFloat(cover, "rotation", 0f, 360f);
        rotateAnimation.setDuration(5000);
        rotateAnimation.setRepeatCount(ObjectAnimator.INFINITE);
        rotateAnimation.setRepeatMode(ObjectAnimator.RESTART);
        rotateAnimation.setInterpolator(new LinearInterpolator());
    }

    public void setCover(Uri cover) {
        VinylView.this.cover.setImageResource(R.drawable.vinyl);
        Glide.with(context)
                .load(cover)
                .asBitmap()
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.vinyl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        if (resource == null) {
                            VinylView.this.cover.setImageResource(R.drawable.vinyl);
                        } else {
                            VinylView.this.cover.setImageBitmap(resource);
                        }
                        if (shouldGeneratePallete) {
                            Palette.from(resource)
                                    .generate(new Palette.PaletteAsyncListener() {
                                        @Override
                                        public void onGenerated(Palette palette) {
                                            Palette.Swatch textSwatch = palette.getVibrantSwatch();
                                            if (textSwatch == null) {
                                                return;
                                            }
                                            int wave1 = palette.getVibrantSwatch().getRgb();//palette.getMutedSwatch().getRgb();
                                            float[] hsv = new float[3];
                                            Color.colorToHSV(wave1, hsv);
                                            int wave2 = Color.HSVToColor(new float[]{hsv[0], hsv[1] - 0.15f, hsv[2]}); //palette.getLightMutedSwatch().getRgb(); //Color.rgb(Color.red(wave2), Color.green(wave2) - 11, Color.blue(wave2) - 39);
                                            int wave3 = Color.HSVToColor(new float[]{hsv[0] + 0.03f, hsv[1] - 0.45f, hsv[2]}); //palette.getVibrantSwatch().getRgb(); //Color.rgb(Color.red(wave2), Color.green(wave2) + 32, Color.blue(wave2) + 78);
                                            int wave4 = Color.HSVToColor(new float[]{hsv[0] + 0.03f, hsv[1] - 0.60f, hsv[2]}); //palette.getLightVibrantSwatch().getRgb(); //Color.rgb(Color.red(wave3), Color.green(wave3) + 4, Color.blue(wave3) + 39);
                                            EventBus.getDefault().post(new PalleteEvent(wave4, wave3, wave2, wave1));
                                        }
                                    });
                        }
                    }
                });
    }

    public void resumeAnim() {
        if (rotateAnimation.isStarted()) {
            rotateAnimation.resume();
        } else {
            rotateAnimation.start();
        }
    }

    public void pauseAnim() {
        rotateAnimation.pause();
    }

    public void setShouldGeneratePallete(boolean shouldGeneratePallete) {
        this.shouldGeneratePallete = shouldGeneratePallete;
    }
}
