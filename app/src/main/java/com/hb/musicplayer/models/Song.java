package com.hb.musicplayer.models;

import android.content.ContentUris;
import android.net.Uri;

import java.io.Serializable;

/**
 * Created by HB on 09.08.2017.
 */

public class Song implements Serializable{

    private String id;
    private String title;
    private String artist;
    private String cover;
    private long duration;

    public Song(String id, String title, String artist, String cover, long duration) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.cover = cover;
        this.duration = duration;
    }

    public String getId() {
        Uri idUri = ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Long.parseLong(id));
        return idUri.toString();
    }

    public String getJustId(){
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Song song = (Song) o;

        if (duration != song.duration) return false;
        if (id != null ? !id.equals(song.id) : song.id != null) return false;
        if (title != null ? !title.equals(song.title) : song.title != null) return false;
        if (artist != null ? !artist.equals(song.artist) : song.artist != null) return false;
        return cover != null ? cover.equals(song.cover) : song.cover == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (artist != null ? artist.hashCode() : 0);
        result = 31 * result + (cover != null ? cover.hashCode() : 0);
        result = 31 * result + (int) (duration ^ (duration >>> 32));
        return result;
    }
}
