# MusicPlayer


MusicPlayer is an Android application for phones for viewing and playing music from the phones
storage. It focuses on visual design and animations.
 
Demo & Screenshots:
![Alt text](https://68.media.tumblr.com/829dd493b0b9e9f56dd795bfcc7992d9/tumblr_owaj17eUCf1vuan0ro2_540.gif "Demo")
![Alt text](https://68.media.tumblr.com/7584aa35c2c92c990b1ecde142c51b80/tumblr_owaj17eUCf1vuan0ro1_540.png "Screenshots")